var express = require('express');
var router = express.Router();

var r = require('rethinkdb');

router.get('/:client/:count/:tempo', function (req, res, next) {
  var data = {
    client: req.param('client'),
    count: req.param('count'),
    tempo: req.param('tempo'),
    timestamp: new Date()
  };
  r.connect({host: 'hackathon201602.yustam.jp', port: 28015}, function (err, conn) {
    if (err) throw err;
    r.db('recojog').table('jogging').insert(data).run(conn, function (err, res) {
      if (err) throw err;
      console.log(res);
    });
  });
  res.send(data);
});

router.get('/:client', function (req, res, next) {
  res.send('hogehoge');
});

module.exports = router;
