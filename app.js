var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var r = require('rethinkdb');

var routes = require('./routes/index');
var api = require('./routes/api');

var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
server.listen(5000);

var emitter = require('socket.io-emitter')({host: '127.0.0.1', port: 6379});


// view engine setup
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

io.on("connection", function (socket) {
  console.log("online id: " + socket.id);
  socket.broadcast.emit("online", {id: socket.id});
  socket.on("disconnect", function () {
    console.log("offline id: " + socket.id);
    socket.broadcast.emit("offline", {id: socket.id});
  });
  socket.on("message", function (obj) {
    var id = socket.id;
    var message = obj.message;
    console.log("message id: " + id + " message: " + message);
    io.emit("message", {id: id, message: message});
  });
});

r.connect({host: 'hackathon201602.yustam.jp', port: 28015}, function (err, conn) {
  if (err) throw err;
  r.db('recojog').table('jogging').changes().run(conn, function (err, cursor) {
    if (err) throw err;
    cursor.each(function (err, data) {
      if ('new_val' in data) {
        io.emit('jogging', data['new_val']);
      }
    });
  });
});
